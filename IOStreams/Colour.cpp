#include "Colour.h"

Colour::Colour()
{
    red = MAX;
    blue = MAX;
    green = MAX;
}

Colour::Colour(unsigned char r, unsigned char g, unsigned char b)
{
    setRed(r);
    setGreen(g);
    setBlue(b);
}

Colour::~Colour()
{

}

void Colour::setRed(unsigned char red)
{
    this->red = red;
}

unsigned char Colour::getRed()
{
    return this->red;
}

void Colour::setGreen(unsigned char green)
{
    this->green = green;
}

unsigned char Colour::getGreen()
{
    return green;
}

void Colour::setBlue(unsigned char blue)
{
    this->blue = blue;
}

unsigned char Colour::getBlue()
{
    return blue;
}

std::ostream& operator<< (std::ostream &out, const Colour &colour)
{
    // Since operator<< is a friend of the Colour class, we can access Colour's members directly.
    // have to cast these to ints for output. 
    out << "Colour(" << (int)colour.red << ", " << (int)colour.green << ", " << (int)colour.blue << ')'; // actual output done here
 
    return out; // return std::ostream so we can chain calls to operator<<
}