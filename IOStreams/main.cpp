#include <iostream> //for cout/endl etc.
#include <cstdlib>  //c++ version of stdlib - for exit. 

#include "Colour.h" //Need this to define our class. 

using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
    cout << "Testing iostreams " << endl;

    Colour foreground(255,0,0);

    cout << foreground << endl;

    exit(0);
}
