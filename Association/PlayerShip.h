#ifndef _PLAYERSHIP_H
#define _PLAYERSHIP_H

class Colour;
class PowerUp;
class Vector2f;

class PlayerShip 
{
  public:
    static const int MAX_INVENTORY = 10;

  private:
    int energy;
    Vector2f* position;
    Colour* colour;
    PowerUp* inventory[MAX_INVENTORY];
    int itemCount;

  public:
    static const int MAX_ENERGY;

    PlayerShip();
    PlayerShip(Vector2f *pos, Colour* col);
    ~PlayerShip();

    void takeHit(int points);

    Colour* getColour();
    void setColour(Colour* value);

    Vector2f* getPosition();
    void setPosition(Vector2f* value);

    void collectPowerUp(PowerUp* powerUp);
    
    //for the examples
    void printShipInfo(); 
};


#endif
