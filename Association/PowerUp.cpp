
#include "PowerUp.h"

PowerUp::PowerUp()
{
    name = nullptr;
}


PowerUp::~PowerUp()
{

} 

PowerUp::PowerUp(string name)
{
    this->setName(name);
}

void PowerUp::setName(string name)
{
    this->name = name;
}

string PowerUp::getName()
{
    return name;
}
