#include "Colour.h"
#include <cmath>
const float Colour::FP_ERROR = 0.01f;

Colour::Colour()
{
    red = MAX;
    blue = MAX;
    green = MAX;
}

Colour::Colour(unsigned char r, unsigned char g, unsigned char b)
{
    setRed(r);
    setGreen(g);
    setBlue(b);
}

Colour::~Colour()
{

}

void Colour::setRed(unsigned char red)
{
    this->red = red;
}

unsigned char Colour::getRed()
{
    return this->red;
}

void Colour::setGreen(unsigned char green)
{
    this->green = green;
}

unsigned char Colour::getGreen()
{
    return green;
}

void Colour::setBlue(unsigned char blue)
{
    this->blue = blue;
}

unsigned char Colour::getBlue()
{
    return blue;
}

// This is automatically called when '==' is used with
// between two Colour objects
bool Colour::operator==(Colour const& obj)
{
    if(fabs(red-obj.red) < FP_ERROR)
        if(fabs(green-obj.green) < FP_ERROR)
            if(fabs(blue-obj.blue) < FP_ERROR)
                return true;
            else 
                return false;
        else   
            return false;
    else    
        return false;

    return false;
}