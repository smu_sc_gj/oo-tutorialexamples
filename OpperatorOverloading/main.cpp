#include <iostream> //for cout/endl etc.
#include <cstdlib>  //c++ version of stdlib - for exit. 

#include "Colour.h" //Need this to define our class. 

using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
    cout << "Testing the colour library" << endl;

    Colour foreground; //static colour on stack.
    foreground.setRed(255);
    foreground.setBlue(255);
    foreground.setGreen(255);

    //cout can't print an unsigned int use + to make promote it to a signed char.
    cout << +foreground.getRed() << "," 
        << +foreground.getGreen() << "," 
        << +foreground.getBlue() << endl;

    Colour background; //static colour
    background.setRed(0);
    background.setBlue(255);
    background.setGreen(0);

    cout << +background.getRed() << "," 
        << +background.getGreen() << "," 
        << +background.getBlue() << endl;

    Colour white(255,255,255); //static colour
    cout << +white.getRed() << "," 
        << +white.getGreen() << "," 
        << +white.getBlue() << endl;

    if(background == white)
        cout << "The background is white!" << endl;
    
    if(foreground == white)
        cout << "The foreground is white!" << endl;
}
