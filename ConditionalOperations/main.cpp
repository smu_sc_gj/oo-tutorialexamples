#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
    int marks = 40;
    int passCount = 0;
    int failCount = 0;

    std::string result = (marks >= 40) ? "passed" : "failed";
    ((marks >=40) ? passCount : failCount)++;

    std::cout << "The string is " << result << std::endl;
    std::cout << "Pass Count = " << passCount << " Fail Count = " << failCount << std::endl;

    marks = 10; 

    result = (marks >= 40) ? "passed" : "failed";
    ((marks >=40) ? passCount : failCount)++;

    std::cout << "The string is " << result << std::endl;
    std::cout << "Pass Count = " << passCount << " Fail Count = " << failCount << std::endl;
}
