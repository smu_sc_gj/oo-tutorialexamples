# README #

Example code for the object orientated notes in CIS4008, originally contained just objects, association and inheritance but has now been extended to include some template examples. 

## Example List ##

1. Objects
1. Inheritance
2. Association
3. IOStreams
4. References
5. Conditional Operators
5. Templates
6. Standard Template Library - Collections
7. Smart Pointers
8. Expanded Example