/*
 * Circle.h
 *
 *  Created on: 8 Jun 2011
 *      Author: sc-gj
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_

class Circle {
public:
	Circle();
	virtual ~Circle();
	void setRadius(float);
	float getRadius(void);
	
private:
	float radius;
};



#endif /* CIRCLE_H_ */
