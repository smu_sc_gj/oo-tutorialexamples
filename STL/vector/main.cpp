
#include <iostream>
#include <cstdlib>
#include <vector>

#include "Circle.h"
#include "Square.h"

using std::cout;
using std::endl;

typedef std::vector<Square*> SquareVector;
typedef std::vector<Square*>::iterator SqIt;

int main(int argc, char* argv[])
{
	// Using stl
	Circle* a = new Circle();
	a->setRadius(5);

	Circle* b = new Circle();
	b->setRadius(4);

	std::vector<Circle*> circleList;
	circleList.push_back(a);
	circleList.push_back(b);

	for(unsigned int i = 0; i < circleList.size(); i++)
		cout << "Circle radius " 
		     << circleList[i]->getRadius() 
			 << endl;

	std::vector<Circle*>::iterator it;

	for(it = circleList.begin(); it != circleList.end(); it++)
		cout << "Circle radius " 
		     << (*it)->getRadius() 
			 << endl;

	Square* c = new Square();
	c->setLenght(5);

	Square* d = new Square();
	d->setLenght(4);

	SquareVector squareList;
	squareList.push_back(c);
	squareList.push_back(d);

	for(unsigned int i = 0; i < squareList.size(); i++)
		cout << "Square side length " 
		     << squareList[i]->getLenght() 
			 << endl;


	for(SqIt it2 = squareList.begin(); it2 != squareList.end(); it2++)
		cout << "Square side length " 
		     << (*it2)->getLenght() 
			 << endl;

	exit(0);
}