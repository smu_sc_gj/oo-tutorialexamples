
#include "OnScreenObject.h"
#include "Vector2.hpp"

OnScreenObject::OnScreenObject()
{
    position.x(0.0f);
    position.y(0.0f);
}

OnScreenObject::~OnScreenObject()
{

}

OnScreenObject::OnScreenObject(Vector2<float>& position)
{
    setPosition(position);
}

void OnScreenObject::setPosition(Vector2<float>& value) 
{
    position = value;
}

Vector2<float>& OnScreenObject::getPosition() {
    return position;
}

