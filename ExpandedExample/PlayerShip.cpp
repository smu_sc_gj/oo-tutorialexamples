
#include "PlayerShip.h"
#include "Colour.h"
#include "PowerUp.h"
#include "Vector2.hpp"

#include <cstdlib>
#include <iostream>
using std::cout;
using std::endl;

const int PlayerShip::MAX_INVENTORY;

const int PlayerShip::MAX_ENERGY= 100;

PlayerShip::PlayerShip() : OnScreenObject() 
{
    energy = MAX_ENERGY;
    colour = nullptr;
}

PlayerShip::PlayerShip(Vector2<float>& pos, std::shared_ptr<Colour> col) : OnScreenObject(pos) 
{
    energy = MAX_ENERGY;
    setColour(col);
}

PlayerShip::~PlayerShip()
{
    colour.reset();

   for(std::shared_ptr<PowerUp> thing : inventory)
   { 
       thing.reset();
   }
}

void PlayerShip::takeHit(int points) 
{
    energy -= points;
}

std::shared_ptr<Colour> PlayerShip::getColour() 
{
    return colour;
}

void PlayerShip::setColour(std::shared_ptr<Colour> value) 
{
    colour = value;
}

void PlayerShip::collectPowerUp(std::shared_ptr<PowerUp> powerUp) 
{
    /* Some power ups may change energy, 
     * code to handle this would go here
     */

    inventory.push_back(powerUp);
}

// Note: This is only for the class examples 
// * adding test code like this into our classes
// * increases the coupling (e.g. to cout etc.)
// * we'd normally use friend clasesses or a
// * test framework 

void PlayerShip::printShipInfo() 
{
    cout << "** Player Ship **" << endl;
    
    if(colour != nullptr)
        cout << "Colour is:(R: " << +colour->getRed() << 
                " , G: " << +colour->getGreen() << 
                " , B: " << +colour->getBlue() << 
                " )" << endl;

    
    cout << "Position is:(X: " << position.x() << 
            " , Y: " << position.y() << 
            " )" << endl;
    
    cout << "Ship Inventory:" << endl;

    for(int i = 0; i < inventory.size(); i++)
    {
        cout << "\t" << inventory[i]->getName() << endl;
    }
    
}

void PlayerShip::draw() 
{
    //Draw code here.
    cout << "DEBUG: Drawing PlayerShip" << endl;
}

