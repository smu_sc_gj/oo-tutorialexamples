#ifndef _POWERUP_H
#define _POWERUP_H


#include "OnScreenObject.h"
#include <string>
using namespace std;

#include "Vector2.hpp"

class PowerUp : public OnScreenObject 
{
  private:
    string name;

  public:
    PowerUp();
    explicit PowerUp(string name);
    PowerUp(Vector2<float>& postion, string name);
    ~PowerUp();

    void setName(string name);
    string getName();

    void draw();

};
#endif
