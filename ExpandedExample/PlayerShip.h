#ifndef _PLAYERSHIP_H
#define _PLAYERSHIP_H

#include <vector>
#include <memory>

#include "OnScreenObject.h"
#include "Vector2.hpp"

class Colour;
class PowerUp;

class PlayerShip : public OnScreenObject {
  public:
    static const int MAX_INVENTORY=  10;

  private:
    int energy;
    std::shared_ptr<Colour> colour;

    std::vector<std::shared_ptr<PowerUp>> inventory;

  public:
    static const int MAX_ENERGY;

    PlayerShip();
    PlayerShip(Vector2<float>& pos, std::shared_ptr<Colour> col);
    ~PlayerShip();

    void takeHit(int points);

    std::shared_ptr<Colour> getColour();
    void setColour(std::shared_ptr<Colour> value);

    void collectPowerUp(std::shared_ptr<PowerUp> powerUp);

    //for the examples
    void printShipInfo();

    void draw();

};
#endif
