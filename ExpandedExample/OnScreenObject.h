#ifndef _ONSCREENOBJECT_H
#define _ONSCREENOBJECT_H


#include "Vector2.hpp"

using Maths::Vector2;

class OnScreenObject 
{
  protected:
    Vector2<float> position;


  public:
    OnScreenObject();

    virtual ~OnScreenObject();

    OnScreenObject(Vector2<float>& position);

    void setPosition(Vector2<float>& value);

    Vector2<float>& getPosition();

    virtual void draw() = 0;   //Pure virtual method

};
#endif
