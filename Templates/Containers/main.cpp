#include <iostream>
#include <vector>
#include <chrono> // std::crono
#include <random> // std::mt19937 (PRNG)
#include <algorithm>  // std::sort

using std::vector;

int main(int argc, char* argv[])
{
    const int TEST_SIZE = 10;

    //static vector. 
    vector<float> floats;

    // Pre-allocate to speed things up. 
    floats.resize(TEST_SIZE);

    // Hardware random device. 
    std::random_device rd;

    // Seeding the Mersenne Twister PRNG (Pseudorndom Number Generator) 
    // https://en.wikipedia.org/wiki/Mersenne_Twister
    // Combining the hardware random device, 
    // system clock and high resolution clock as a 
    // start point. 
    std::chrono::seconds sysClock = std::chrono::duration_cast<std::chrono::seconds>(
                                        std::chrono::system_clock::now().time_since_epoch()
                                        );
    std::chrono::microseconds hiResClock = std::chrono::duration_cast<std::chrono::microseconds>(
                                                std::chrono::high_resolution_clock::now().time_since_epoch()
                                            );

    std::mt19937::result_type seed = rd() ^ ((std::mt19937::result_type) (sysClock + hiResClock).count());

    // Thats a lot of work for a good seed!
    std::mt19937 gen(seed);

    // Create a uniform distribution
    std::uniform_real_distribution<float> distrib(-10.0f, 10.0f);

    for( unsigned long j = 0; j < TEST_SIZE; ++j )
    {
        floats.push_back(distrib(gen));
    }

    std::cout << "Unsorted Vector" << std::endl;
    // one way to print 
    for (float f : floats)
        std::cout << f << ' ';
    std::cout << std::endl;

    //sort the vector
    //3rd paramter can be a std function or our own. 
    std::sort(floats.begin(), floats.end(), std::greater<float>());
   
    std::cout << "Sorted Vector" << std::endl;
    // another way to print 
    for (std::vector<float>::iterator it=floats.begin(); it!=floats.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << std::endl;
    
}
