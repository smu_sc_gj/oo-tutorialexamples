#include <iostream>


// based on https://www.cplusplus.com/doc/oldtutorial/templates/

template <class myType>
myType getMax (myType a, myType b) 
{
    if(a>b)
        return a;
    else
        return b;
}

/* if shorthand version 
template <class myType>
myType getMax (myType a, myType b) 
{
    condition ? true value : false value
            v    v
    return (a>b?a:b);
}
*/

// More common template name
// class vs typname in template definition 
// have the same effect. 
template <typename T>
T getMin (T a, T b) 
{
 return (a<b?a:b);
}


int main(int argc, char* argv[])
{
    std::cout << "Hello World" << std::endl;

    int a = 10;
    int b = 5;

    std::cout << "The larger of " << a << " and " << b << " is " << getMax(a, b) << std::endl;
    std::cout << "The smaller of " << a << " and " << b << " is " << getMin(a, b) << std::endl;

    float x = 10.0f;
    float y = 5.0f;

    std::cout << "The larger of " << a << " and " << b << " is " << getMax(a, b) << std::endl;
    std::cout << "The smaller of " << a << " and " << b << " is " << getMin(a, b) << std::endl;

    
}
