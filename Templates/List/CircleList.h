/*
 * CircleList.h
 *
 *  Created on: 8 Jun 2011
 *      Author: sc-gj
 */

#ifndef CIRCLE_LIST_H_
#define CIRCLE_LIST_H_

class Circle;

class CircleList 
{
private:
	static const int LIST_MAX = 10;
	Circle* list[LIST_MAX];
	int size;
public:
	CircleList();
	virtual ~CircleList();

	void addList(Circle* newSq);
	Circle* elementAt(int index);

	void print();
};

#endif /* CIRCLE_LIST_H_ */
