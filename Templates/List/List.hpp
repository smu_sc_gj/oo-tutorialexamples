/*
 * List.hpp
 *
 *  Created on: 8 Jun 2011
 *      Author: sc-gj
 */

#ifndef MYLIST_HPP_
#define MYLIST_HPP_

template <typename T>

class MyList
{
private:
	static const int LIST_SIZE = 10;
	T list[LIST_SIZE];
	int size;
public:

	MyList() 
	{
		size = 0;
	};

	~MyList() 
	{
		for(int i = 0; i < size; i++)
		{
			delete list[i];
		}
	}

	void addList(T& a)
	{
		list[size]=a;
		size++;
	}

	T& elementAt(int i)
	{
		return list[i];
	}
};

#endif
