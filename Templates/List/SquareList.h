/*
 * SquareList.h
 *
 *  Created on: 8 Jun 2011
 *      Author: sc-gj
 */

#ifndef SQUARE_LIST_H_
#define SQUARE_LIST_H_

class Square;

class SquareList 
{
private:
	static const int LIST_MAX = 10;
	Square* list[LIST_MAX];
	int size;
public:
	SquareList();
	virtual ~SquareList();

	void addList(Square* newSq);
	Square* elementAt(int index);

	void print();
};

#endif /* SQUARE_LIST_H_ */
