/*
 * SquareList.cpp
 *
 *  Created on: 8 Jun 2011
 *      Author: sc-gj
 */

#include "CircleList.h"
#include "Circle.h"

CircleList::CircleList()
{
	size = 0;
}

CircleList::~CircleList()
{
	for(int i = 0; i < size; i++)
	{
		delete list[i];
	}
}

void CircleList::addList(Circle* newSq)
{
	list[size]=newSq;
	size++;
}

Circle* CircleList::elementAt(int index)
{
	return list[index];
}

