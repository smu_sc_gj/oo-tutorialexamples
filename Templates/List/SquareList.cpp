/*
 * SquareList.cpp
 *
 *  Created on: 8 Jun 2011
 *      Author: sc-gj
 */

#include "SquareList.h"
#include "Square.h"

SquareList::SquareList()
{
	size = 0;
}

SquareList::~SquareList()
{
	for(int i = 0; i < size; i++)
	{
		delete list[i];
	}
}

void SquareList::addList(Square* newSq)
{
	list[size]=newSq;
	size++;
}

Square* SquareList::elementAt(int index)
{
	return list[index];
}

