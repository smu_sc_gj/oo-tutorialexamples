

#include <iostream>
#include <cstdlib>

#include <memory> // for shared ptr
#include <vector> // for std::vector

#include "Square.h"
#include "Vector2.hpp"

using std::cout;
using std::endl;

using Maths::Vector2;

void aPointlessFunction(std::shared_ptr<Square> circle)
{
	circle->setLenght(100);

	// how many things point to c?
	cout << "Ref count (in the function): " << circle.use_count() << endl;
}

int main(int argc, char* argv[])
{
	Square* c = new Square();
	c->setLenght(5);

	std::shared_ptr<Square> cShared(c);
	
	// use the shared pointer like the raw one
	cout << "Length of c " << c->getLenght() << endl;
	cout << "Length of c " << cShared->getLenght() << endl;

	// how many things point to c?
	cout << "Ref count (after creating cShared): " << cShared.use_count() << endl;

	// create another pointer to the same object
	std::shared_ptr<Square> anotherC = cShared;

	// how many things point to c?
	cout << "Ref count (after creating anotherC): " << cShared.use_count() << endl;

	// calling the function passing c by shared_ptr
	aPointlessFunction(cShared);

	// New c lenght
	cout << "Length of c (after function): " << cShared->getLenght() << endl;

	// how many things point to c?
	cout << "Ref count (after creating anotherC): " << cShared.use_count() << endl;

	// cleanup
	anotherC.reset();
	cShared.reset();

	// Is c valid ? - No deleted!!
	cout << "Length of c " << c->getLenght() << endl;

	// Better initialisation
	std::shared_ptr<Square> d(new Square());
	d->setLenght(3);

	// Vector of smart pointers

	std::vector<std::shared_ptr<Square>> pointerList;

	pointerList.push_back(d);
	pointerList.push_back(std::shared_ptr<Square>(new Square(4.0f)));

	for(std::shared_ptr<Square> sq : pointerList) 
		cout << sq->getLenght() << ", ";
	cout << endl;
    
    // Vector of smart pointers to templated classes
    
    // Make some vectors!!
	std::shared_ptr<Vector2<int>> one(new Vector2<int>(1,1));
	std::shared_ptr<Vector2<int>> two(new Vector2<int>(2,2));
    std::shared_ptr<Vector2<int>> three(new Vector2<int>(3,3));

	// Vector of smart pointers

	std::vector<std::shared_ptr<Vector2<int>>> vectorList;

	vectorList.push_back(one);
	vectorList.push_back(two);
    vectorList.push_back(three);
                                        
	for(std::shared_ptr<Vector2<int>> vect : vectorList) 
		cout << *vect << ", ";
	cout << endl;
    

	exit(0);
}